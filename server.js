const express = require('express');
const app = express();
const fs = require('fs'); 
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json();
const md5 = require("blueimp-md5");
app.use(bodyParser.json({type: 'application/*+json'}));


//login
app.get('/login', function(req, res) {
    fs.readFile(__dirname + '/' + 'users.json', 'utf8', function(err, data) {
        var  users = JSON.parse(data); 
        
        var use;
        let query = req.query;
        for ( user in users) {
            
            console.log(query.email + '-' + users[user].email);
            console.log(query.password + '-' + users[user].password);
            if ((query.email === users[user].email) && (query.password === users[user].password))  {
                use = users[user];
                console.log(use);
               //res.end( "Login as user:\n" + JSON.stringify(users[user], null, 4));
             } 
        }

        if (use) {
            res.end( "Login as user:\n" + JSON.stringify(use, null, 4));
        } else {
            res.end('Wrong Password!')
        }
});
});

//register
app.post('/register', jsonParser, function(req, res) {
        fs.readFile(__dirname + '/' + 'users.json', 'utf8', function(err, data) {
        let users = JSON.parse(data);
        let body = req.body;
        let newuser = {};
    
        newuser.id = body.id;
        newuser.name = body.name;
        newuser.password = body.password;
        newuser.confirm = body.confirm;
        newuser.email = body.email;
        newuser.profession = body.profession;
        
        if(newuser.password === body.confirm){
            users["user" + body.id] = newuser;
            res.end("Register Successfully: \n" + JSON.stringify(newuser, null, 4));
            fs.writeFileSync(__dirname + '/' + 'users.json', JSON.stringify(users, null, 4));
        }else{
            res.end("Please check confirm password!: \n");
        }
    });   
});

//update
app.put('/update/:id', jsonParser, function(req, res) {
    fs.readFile(__dirname + '/' + 'users.json', 'utf8', function(err, data) {    
        var users = JSON.parse(data); 
        const id_update = req.params.id;
        var body = req.body;
        for (user in users) {
            if ((id_update == users[user].id)) {
                users[user].name = body.name;
                users[user].password = body.password;
                users[user].profession = body.profession;
                console.log(id_update);
             } 
        }
        fs.writeFileSync(__dirname + '/' + 'users.json', JSON.stringify(users, null, 4));
        res.end(JSON.stringify(users, null, 4));
    });    
});

//forgotpass
app.put('/forgot', jsonParser, function(req, res) {
    fs.readFile(__dirname + '/' + 'users.json', 'utf8', function(err, data) {
        var users = JSON.parse(data); 
        var body = req.body;
        var token;
        var id; 
        for (user in users) {
            if (body.email == users[user].email) {
                token = md5(body.email);
                id = users[user].id;
            }
        }
        if(token) {
            res.end(JSON.stringify(`http://localhost:8081/forgotactive?token=${token}&id=${id}`));
        } else {
            res.end(JSON.stringify(`Wrong Email`));
        }

    });
});

//forgotandupdate
app.get('/forgotactive', jsonParser, function(req, res) {
    fs.readFile(__dirname + '/' + 'users.json', 'utf8', function (err, data) {
        var users = JSON.parse(data);
        var query = req.query;
        var acc;
        for (user in users) {
           
            if ((query.id == users[user].id) && (query.token == md5(users[user].email))) {
                
                users[user].password = '123';
                acc = users[user];
                
            }
        }
        console.log(acc);
        if(acc) {
            fs.writeFileSync(__dirname + '/' + 'users.json', JSON.stringify(users, null, 4));
            res.end(`Default password: 123 \n
                    Change your password: http://localhost:8081/update/${acc.id}
            `);
        } else {
            res.end('Token out of time!')
        }
    });
});

const server = app.listen(8081, function() {
    const host = server.address().address
    var port = server.address().port
    console.log('Exp app listening at http://%s:%s', host, port)
})
